def reverser
  if string_size(yield) == 1
    yield.reverse

  else
    arr = yield.split
    string_size(yield).times {|i| arr[i]=arr[i].reverse }
    arr.join(" ")
  end
end

#Helper method for reverser
def string_size(string)
  string.split.size
end

def adder(amount=nil)
  if amount == nil
    yield + 1
  else
    yield + amount
  end
end

def repeater(num_of_times=nil)
  yield if num_of_times == nil
  num_of_times.times { yield } unless num_of_times == nil
end
