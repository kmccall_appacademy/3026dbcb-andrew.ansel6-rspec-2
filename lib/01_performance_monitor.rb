require "time"

def measure(counter=1)
  start_time = Time.now
  counter.times { yield }
  (Time.now-start_time) / counter
end
